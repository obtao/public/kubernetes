# Gitlab-ci

Ces dernières année, **Github s'est révélé être le dépôt préféré du monde Opensource.**

Pendant ce temps son petit frère, **Gitlab, a largement progressé**. 
Il propose désormais des fonctionnalités intégrées plus que **prometteuses pour les professionnels**. 

Bref, vous l'aurez compris, **on aime Gitlab**. Un simple fichier, le `.gitlab-ci.yaml` vient définir simplement les étapes d'intégration et déploiement continus.

Dans cette article nous allons développer l'utilisation de **Gitlab-ci dans un contexte Symfony** :

* La construction d'un **socle docker** `php-fpm` (avec Blackfire intégré)
* La construction d'un **socle docker** `nginx`
* L'intégration continue d'une **application complète sous Symfony4**. (**Construction des images**, **build**, **test**)

Si vous suivez les articles sur Kubernetes, vous saurez aussi vous en servir pour **déployer vos applications dans un cluster Kubernetes** en utilisant Helm.


## Socle

L'idée est de créer un sous-projet Gitlab contenant tous les socles *Docker* dont vous aurez besoin :

* php-fpm
* nginx
* redis
* ...

Gitlab-ci se chargera pour vous de builder les images, et grâce au *Docker Registry* intégré dans Gitlab, vous pourrez stocker toutes vos images. 

Vous pouvez avoir un aperçu de ce que peut donner les fichiers `.gitlab-ci.yml` et `Dockerfile` dans les exemples :

* [php-fpm](https://gitlab.com/obtao/docker-images/php-fpm) 
* [nginx](https://gitlab.com/obtao/docker-images/nginx)

Les `.gitlab-ci.yaml` de ces projets sont simples. 
Lors d'un push sur `master` et lors de la création d'un tag, gitlab-ci viendra builder l'image. 
Pour un push sur `master`, gitlab-ci viendra tagguer l'image en `latest`
Pour la création d'un tag, gitlab-ci viendra tagguer l'image avec le nom du tag. 


```yaml
stages:
  - build

docker:
  stage: build
  image: docker:stable
  only:
    - /^master$/
    - tags
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build -t $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG:-latest} .
    - docker push $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG:-latest}
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2

  services:
    - docker:dind

```

Les variables d'environnement utilisées dans ce fichier sont fournies directement par Gitlab lors de l'execution du job. 

Et... c'est tout. 

Si vous le souhaitiez, vous pourriez ajouter des build/test des images lors du push sur un pattern de branche. 

## Création du dépôt Symfony4 

Le dépôt Symfony4 n'est pas différent d'un autre dépôt Symfony4. 

Nous y ajoutons le fichier `.gitlab-ci.yaml` qui vient définir les actions d'intégration continue. 
Le principe est le même, selon ce qui est poussé sur git, gitlab viendra builder les images docker associées


* Un commit sur `master` viendra builder l'image `latest`
* Un tag `1.0.0` viendra builder l'image `1.0.0`
* Un commit sur une branche `hotfix/*`, `release/*` ou `feature/*` viendra builder une image `feature-ma-feature` par exemple. 


Sur ces images, nous pourrons lancer les tests. 

```yaml
stages:
  - build
  - tests

before_script:
  - export APP_VERSION=`echo ${CI_COMMIT_REF_SLUG:-latest} | sed "s/^master$/latest/g"`

.docker-job:
  image:
    name: docker/compose:1.24.0
    entrypoint: [""]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind

#########
# BUILD #
#########
.build-image:
  extends: .docker-job
  stage: build
  only:
    - /^master$/
    - /^(hotfix|feature|release).*$/
    - tags
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker-compose build blog-${CONTAINER}
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker-compose push blog-${CONTAINER}

build-php:
  extends: .build-image
  variables:
    CONTAINER: php

build-nginx:
  extends: .build-image
  variables:
    CONTAINER: nginx
    
tests:
  extends: .docker-job
  stage: tests
  only:
    - /^master$/
    - /^(hotfix|feature|release).*$/
    - tags
  script:
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY}
    - docker-compose pull blog-php
    - docker-compose run -T blog-php php-cs-fixer fix --dry-run

```
 
Allez-y ! Faites vos tests. 
Une fois vos images présentes dans votre registry, vous pourrez les déployer facilement via `Docker` ou `Kubernetes`
