# Déploiement sur un serveur de recette en https

## Contexte

Le https est devenu une norme sur internet. On comprend pourquoi !
Qui a envie de voir ses données transiter en clair sur les réseaux ? 
Qui n'est pas rassuré par ce petit cadenas qui apparait sur les navigateurs lorsqu'un site est sécurisé ?

Personne. Google lui même privilégie les sites en https pour le référencement. 

Avec l'arrivée de Let's Encrypt, posséder son certificat n'a jamais été aussi simple.
Un acteur externe certifie qui vous êtes et vous transmet un certificat. 

### Sans Traefik ?

Sans Traefik, la procédure est tout de même fastidieuse. 
Il faut ouvrir le site sur le port 80, installer le certbot, configurer nginx et récupérer le certificat.

N'oubliez surtout pas de mettre une cron en place pour récupérer de nouveaux certificats avant qu'ils n'expirent (90 jours pour Let's encrypt).

### Ok. Et avec Traefik ?

Avec Traefik, ce n'est **que de la configuration**. 
Vous activez Let's Encrypt, et tout se fait tout seul. 


## Configurer Traefik 

Si vous avez suivi la doc en local, vous pouvez l'adapter à votre QA.
Pour activer les certificats, il vous reste quelques actions à réaliser.


### Acme.json

Ce fichier va contenir tout ce qui est relatif à Let's Encrypt (et les certificats)

```bash
$ touch acme.json
$ chmod 600 acme.json
```

### Docker-compose.yml

Bien entendu, il faut maintenant router le port 443 du host vers traefik, et monter le fichier "acme.json" nouvellement créé

```yaml
[...]
    ports:
      - "80:80"
      - "8080:8080"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
      - /opt/traefik/traefik.toml:/traefik.toml
      - './acme.json:/acme.json'
```


### Traefik.toml

Voici les modification à réaliser dans le traefik.toml : 
* Modifier l'entrypoint `http` pour qu'il soit redirigé vers `https`
* Créer l'entrypoint `https` sur le port 443, et activer le tls
* Créer La section `[acme]` pour paramétrer la récupération de certificat :
	* `storage = "acme.json"` : Ou sera stocké localement le fichier contenant les certificats
	* `entryPoint = "https"` : A quel [entrypoint traefik](traefik.md#traefik.toml) seront appliqués les certificats
	* `acmeLogging = true` : Activation des logs (bien pratique en cas de problème)
	* `onHostRule = true` : Génération des certificats sur les règles "frontend" des containers
	* `acme.httpChallenge`: Quel est l'entrypoint utilisé pour valider la propriété du domaine


```ini
defaultEntryPoints = ["https","http"]
	
[entryPoints]
  [entryPoints.http]
  address = ":80"
  [entryPoints.http.redirect]
	entryPoint = "https"

  [entryPoints.https]
  address = ":443"
	[entryPoints.https.tls]

  [entryPoints.admin]
   address=":8080"
   [entryPoints.admin.auth]
	 [entryPoints.admin.auth.basic]
	   users = [
		 "admin:$apr1$Zq$KogYbAwbmeVY5QgrTsHXh0"
	   ]

[docker]
exposedByDefault=false

[api]
entrypoint="admin"
dashboard = true


[acme]
  acmeLogging = true
  onHostRule=true
  email = "contact@votresociete.com"
  storage = "acme.json"
  entryPoint = "https"
[acme.httpChallenge]
  entryPoint= "http"
```

## Conclusion

Redémarrez votre traefik, et c'est terminé. 
Vos sites sont désormais en https, avec des certificats valides. Félicitations! 

En cas de problème, activez le debug de Traefik grace à la configuration `logLevel = "DEBUG"`. 

