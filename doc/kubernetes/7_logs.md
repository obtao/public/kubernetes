# Aggréger les logs (EFK)

## Le problème à résoudre

Les *Pods* sont mortels ! Mais ce n'est pas ce problème que nous allons résoudre. Nous allons composer avec. 

Vous l'avez vu, **il est possible d'aller voir les logs en sortie des containers**. 
Dans l'application Symfony que nous avons déployée, **nous avons même fait en sorte d'écrire les logs JSON sur la sortie standard `stdout`**.

Imaginez maintenant que vous déclarez une *Cronjob*, que le *Job* lance le *Pod*, mais le traitement échoue. 
Le *Pod* finira par disparaître, et **vos logs avec**. 

D'où : Les *Pods* sont mortels. 

Nous allons donc installer la stack **EFK** (Pour *Elasticsearch*, *Fluentd* et *Kibana*).

### Les Objets

### Nodes

Vous êtiez prévenus, **TOUT** est objet dans Kubernetes. Même les *Nodes*. 
Ce sont donc les descriptions des serveurs qui hébergent vos *Pods*, *Services*, ...

#### ConfigMap

Les *ConfigMap* sont des objets **dédiés au stockage de la configuration**. 
Ils pourront être utilisés dans les *Pods** en tant que **Volume (monté en tant que fichiers)** ou en tant que **variables d'environnement**.

### Job

Les objet de type *Job* sont des configurations **permettant de lancer des *Pods* a durée de vie finie**. 
On peut comparer les *Jobs* à des traitement Batch.

En Symfony, nous viendrions par exemple lancer un *Job* qui exécutera une commande Symfony (**Récupération de données**, **traitement de files**, ...).

### CronJob

Les objets *CronJob* sont des **configuration de Cron pour des objets** de type *Job*.

Ils viendront lancer à **l'heure fixée** les **traitements définis**, comme cela existe via une `crontab`. (L'objet porte bien son nom...) 

## DaemonSet

Les objets de type *DaemonSet* sont utilisés pour déployer des *Pods* sur chacun des *Nodes*.

Rarement implémenté par des applications métier, le *DaemonSet* est surtout **pratique pour des traitements sur le cluster Kubernetes**.

### Les commandes

#### Kubectl top nodes

Vous l'aurez remarqué sur le *Dasbhoard* Kubernetes, vous pouvez connaître l'état de vos *Nodes*.

Dans Kubernetes, chaque *Pod* peut définir des ressources serveurs qui lui sont alloués : **CPU** et **Mémoire**.
Pour chacune il peut définir un minimum (`request`) et un max (`limit`).

Ces valeurs, **non obligatoires mais conseillées**, permettent aux *Master* de choisir les *Nodes* qui viendront recevoir les *Pods* concernés. 
Elles permettent aussi à **Kubernetes de définir une QoS pour ce *Pod*** (Un pod qui ne définit ni Request, ni Limit sera vite détruit en cas de manque de ressources).

Pour plus de détails vous pouvez lire [la documentation officielle sur la QoS des *Pods*](https://kubernetes.io/docs/tasks/configure-pod-container/quality-service-pod/)

Et pour en savoir plus sur l'état de vos *Nodes* et les utilisations de ressources, vous pouvez utiliser la commande `kubectl top nodes`.

```bash
$ kubectl top nodes
NAME      CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
node-01   404m         21%    3833Mi          72%
node-02   436m         22%    5128Mi          97%
```

#### Kubectl top pods

Pour en savoir un peu plus sur la consommation de chacun des *Pods* il existe `kubectl top pods`

```bash
$ kubectl -n kube-system top pods
  NAME                                             CPU(cores)   MEMORY(bytes)
  canal-j687k                                      26m          57Mi
  canal-vxg7q                                      28m          70Mi
  cert-manager-79dcb77fbf-klmbf                    2m           16Mi
  cert-manager-webhook-85dd96d87-k8qpq             1m           12Mi
  kube-dns-59f6f46c8-dnrfv                         4m           41Mi
  kube-dns-59f6f46c8-rrj2k                         4m           30Mi
  kube-dns-autoscaler-96b4fb4f4-gvvlx              0m           12Mi
  kube-proxy-fwprr                                 3m           18Mi
  kube-proxy-g7mrm                                 3m           21Mi
  kubernetes-dashboard-65c76f6c97-2kn6h            0m           16Mi
  metrics-server-76c55ccbc-t5hv2                   1m           29Mi
  nginx-ingress-controller-84d4dfc9b-69k9v         5m           164Mi
  nginx-ingress-default-backend-7b98d4dc6c-4rnl2   0m           2Mi
  tiller-deploy-7b65c7bff9-fdm9z                   0m           30Mi
  wormhole-6d7wf                                   1m           12Mi
  wormhole-w4dz8                                   4m           22Mi
```

## La pratique !

Nous allons la faire courte. Et 'simplement' installer le chart Helm en adaptant quelques valeurs.
N'oubliez pas de modifier les hosts des **Ingress** dans [le fichier yaml de configuration](../../kubernetes/elastic-stack/values.yaml)

```bash
#Créons le namespace monitoring
#Ajoutons le repo de charts qui contient elastic-stack
$ helm repo add kiwigrid https://kiwigrid.github.io
#Installons elastic-stack
$ helm install --namespace=monitoring --name=elastic-stack stable/elastic-stack -f kubernetes/elastic-stack/values.yaml
# Ajoutez votre authentification basique au namespace monitoring
$ kubectl -n monitoring create secret generic auth-admin --from-file=auth -o yaml
```

Le temps que tout s'installe, je vous propose de détailler les éléments de configuration

* **Kibana** : Ajout d'un *Ingress* et du HTTPS
* **Logstash** : Désactivé (Inutile pour l'instant)
* **Elasticsearch** : Activé sans persistence des données. Cela évite la création de Volumes OVH payants. (Mais si vos *Pods* meurent, les données disparaissent. A éviter en prod)
* **Fluentd** : Activé, branché sur l'elasticsearch, et configuré pour ajouter les données Kubernetes
* **Curator** : Activé, avec un *CronJob* tous les jours à 1h00 pour supprimer les indexes de plus de 7 jours et éviter l'accumulation de logs.

Maintenant que tout est dit, **vous pouvez accéder à votre Dashboard Kibana**.
Il devrait même commencer doucement à se remplir. 
 
**Fluentd est déployé en *DaemonSet***, car il vient parser les sorties des containers de **TOUS** vos *Pods*.



Allez y faire un tour ! 

![Kibana-install](./img/kibana-install.png)

Configurez votre source de données sur les index `logstash-*` et le champ `@timestamp`.

(les champs les plus importants sont `kubernetes.namespace_name` et `kubernetes.container_name`)
Si vous avez configuré vos containers pour que les sorties soient en JSON, vous retrouverez le JSON parsé dans votre Elasticsearch. 
Pour le blog par exemple : 

* Le monolog de notre blog est configuré avec un **JsonFormatter**
* Les logs non tronqués de PHP-FPM 7.3, et la suppression du décorateur étaient indispensables
* Un champ `request_id` est passé sur tout le traitement de la request et ajouté en *extra* dans tous les logs.

![Kibana](./img/kibana.png)

Si vous chargez une des pages du blog, vous pourrez retrouver la trace du traitement dans le champs request_id de Kibana. 

Aussi bien dans les logs nginx, les logs php-fpm et les logs Symfony. **Pratique pour suivre le cheminement de vos requêtes** !

Vous y trouverez **non seulement les logs de vos application**s, mais aussi les **logs de tous les *Pods* que vous avez installés** (Elasticsearch, Kibana, Ingress Controller, ...)

[Allons chercher quelques métriques](./8_prometheus.md).
