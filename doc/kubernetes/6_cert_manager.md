# Ajouter des certificats Let's Encrypt (HTTPS)

## Le problème à résoudre

Vous avez ajouté des **routes**, et vous gérez désormais **des sous domaines** dans votre cluster. 

Si vous utilisiez Docker et Traefik, vous avez appris a apprécier la génération de certificats à la volée via Let's Encrypt.

Si vous n'avez jamais utilisé Let's Encrypt, c'est encore mieux : nous allons **créer des certificats pour chacun des domaines gérés par votre Ingress Controller**.

Pour cela, nous allons installer *Cert-manager*.

### Les Objets

#### Issuer

Nous vous parlions de la possibilité d'étendre Kubernetes avec d'autres types d'objets (des [*CustomerResourcesDefinition* ou *CRD*](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)). 
Les *Issuer* sont apportés par *Cert-Manager*. 

Ils représentent un moyen de récupérer un certificat : 
* **Let's Encrypt**
* **Auto-généré**
* Une **paire de clés** récupérée et stockée dans un *Secret* 

#### ClusterIssuer

Un *ClusterIssuer* n'est ni plus ni moins qu'un *Issuer* disponible pour tout le cluster. 
C'est celui que nous allons utiliser, car tout notre cluster concerne **la même entité vis à vis de Let's Encrypt**. 

Nous en créerons 2 : 
* un *ClusterIssuer* de test, branché sur **les stagings de Let's Encrypt**
* un *ClusterIssuer* de prod, branché sur **la prod de Let's Encrypt**

### Les commandes utiles

### Kubectl explain

Une force de Kubernetes, ce sont ses APIs. Pas seulement parce qu'elles sont complètes et extensibles, mais aussi parce qu'elles sont très bien documentées. 
Tellement bien documentée que **vous pouvez y accéder depuis le terminal**.

C'est le but de la commande `kubectl explain`, **décrire les objets et les paramétrages possible**. 

Par exemple, puisque nous parlons des *Ingress*, allez voir la documentation. 

```bash
$ kubectl explain ingress
```

Vous vous demander comment configurer l'*Ingress* ? Et comment y ajouter un certificat ?
Utiliser les JSONPath pour en savoir plus. 

```bash
$ kubectl explain ingress.spec
$ kubectl explain ingress.spec.tls
```

Attention cependant, si votre Kubernetes n'est pas en version >1.14, vous ne pourrez pas faire de `explain` sur les CRD.

## La pratique !


### Installation des CRD

```bash
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml
```

### Installation du Cert-Manager

Pour installer le *Cert-Manager*, nous utilisons Helm. 

Le paramétrage que nous mettons en place :
* **ingressShim.defaultIssuerName** : L'*Issuer* par défaut (staging)
* **ingressShim.defaultIssuerKind** : Le type de l'*Issuer* par défaut (ClusterIssuer)
* **certmanager.k8s.io/disable-validation** : On désactive la validation du cert-manager sur son propre *Namespace*. ([allez lire la doc](https://docs.cert-manager.io/en/latest/getting-started/install.html#installing-with-regular-manifests))


```bash
$ kubectl create namespace cert-manager
$ kubectl label namespace cert-manager certmanager.k8s.io/disable-validation="true"
$ helm install \
    --name cert-manager \
    --namespace cert-manager \
    --set ingressShim.defaultIssuerName=letsencrypt-staging \
    --set ingressShim.defaultIssuerKind=ClusterIssuer \
    stable/cert-manager
```

Nous pourrions(/devrions) utiliser un fichier yaml versionné pour paramétrer les valeurs.

```
$ helm install \
    --name cert-manager \
    --namespace cert-manager \
    -f ./kubernetes/cert-manager/values.yaml
    stable/cert-manager
```

### Création des Issuer

Créons maintenant les 2 *ClusterIssuer* : `staging` (pour les tests) et `prod` (pour la prod).
Nous utiliserons une vérification [HTTP-01](https://letsencrypt.org/docs/challenge-types/).

Pensez à modifier votre adresse email. (Les fichiers yaml sont également présents [dans le dépôt](../../kubernetes/cert-manager/cert-manager-cluster-issuer.yaml) si vous le souhatez)

```bash
$ echo "
apiVersion: certmanager.k8s.io/v1alpha1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: 'francois@moi.fr'
    privateKeySecretRef:
      name: letsencrypt-staging
    http01: {}
---
apiVersion: certmanager.k8s.io/v1alpha1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: 'francois@moi.fr'
    privateKeySecretRef:
      name: letsencrypt-prod
    http01: {}
 " | kubectl -n cert-manager create -f -
```

### Création d'un Ingress HTTPS

Reprenons l'*Ingress* Nginx créé pendant l'installation de l'**Ingress Controller**. 

Nous allons lui ajouter 3 annotations :

```yaml
metadata:
  annotations:
  	[...]
	#Utilisation du protocole ACME
	kubernetes.io/tls-acme: "true"
	#Utilisation du clusterIssuer de prod créé juste avant
	certmanager.k8s.io/cluster-issuer: "letsencrypt-prod"
	#Redirection de tout le trafic vers le https
	ingress.kubernetes.io/ssl-redirect: "true"
```

Et également configurer le stockage du certificat tls.

```yaml
spec:
  [...]
  tls:
  #Nom du secret qui contiendra le certificat
  - secretName: nginx-prod-tls
    hosts:
    #Nom du host concerné par ce certificat.
    - nginx-test-tls.kubernetes.obtao.com
```

Envoyez la totale (pensez à modifier le nom de domaine): 

```bash
$ echo "
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: 'true'
    certmanager.k8s.io/cluster-issuer: 'letsencrypt-prod'
    ingress.kubernetes.io/ssl-redirect: 'true'
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: auth-admin
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required'
  name: nginx-test
spec:
  rules:
    - host: nginx-test.kubernetes.obtao.com
      http:
        paths:
          - backend:
              serviceName: nginx-test
              servicePort: 80
            path: /
  tls:
  - secretName: nginx-prod-tls
    hosts:
    - nginx-test.kubernetes.obtao.com
 " | kubectl -n test create -f -
```

Patientez le temps que *cert-manager* génère votre certificat.

```bash
$ kubectl -n test get certificate -owide -w
```

Essayez d'accéder à votre nginx, **vous devriez être redirigé vers le https** ! 

[Facilitons maintenant l'accès aux logs](./7_logs.md).
