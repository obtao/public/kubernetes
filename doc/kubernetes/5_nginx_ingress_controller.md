# Gérer des noms de domaines

## Le problème à résoudre

Vous l'avez vu, **déployer est chose aisée avec Kubernetes et Helm**. 
Par contre, le fait d'utiliser les `kubectl proxy` ou `kubectl port-forward` n'a rien de très pratique.

Les *Services* que vous installez sont pour l'instant de type *ClusterIP* (**Disponibles uniquement à l'intérieur du cluster**)
Il existe d'autre types d'objet *Service* : 

* *NodePort*  : Le *Service* est exposé sur un port d'un *Node*. Vous pouvez y accéder via : `http://<NodeIp>:<NodePort>`. Pas très pratique.
* *LoadBalancer* : Crééera dans notre cas un LoadBalancer OVH. **Vite difficile à maintenir lorsque vous installer 10 services**.

### Les Objets

#### Ingress

Pour résoudre ces problèmes, un objet dédié existe, l'*Ingress*. 
Concrêtement, les *Ingress* font le **lien entre l'extérieur et l'intérieur du cluster** (plus précisément vers les *Services*). 

Vous allez pouvoir définir des **routes**, des **hosts**, et des **règles** permettant de transférer des requêtes vers un *Service* donné. 

Par exemple : 

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: blog
  labels:
    app: blog
spec:
  rules:
  - host: blog-demo.kubernetes.obtao.com
    http:
      paths:
      - path: /
        backend:
          serviceName: nginx
          servicePort: http
```

Que l'on peut traduire par : `Router toutes les requêtes http://blog-demo.kubernetes.obtao.com vers le port nommé http dans le service nginx`

Pour interprêter ces objets, nous allons devoir **installer un Ingress Controller** dans notre cluster. 
Pour l'instant nous allons nous contenter de **Nginx Ingress Controller**, un Controller d'Ingress basé sur Nginx.

Vous pourriez en installer un autre, comme par exemple **Traefik**. 

L'installation d'un Ingress Controller va générer **la création d'une entrée DNS *technique*** et d'un **load balancer chez OVH**. 

Si vous avez suivi, c'est l'installation de l'Ingress Controller qui va déployer un *Service* de type *LoadBalancer* (et donc créer un LoadBalancer chez OVH dans notre cas).
Nous allons simplement **créer un CNAME vers ce DNS pour générer facilement des sous-domaines**.

Par exemple : *kubernetes-demo.obtao.com* 

#### Les secret

Les *Secret* existent pour **gérer et stocker des informations sensibles**.
Nous les utiliserons pour stocker des clés/tokens ou mot de passes qui seront exposés en variables d'environnement dans nos containers.


### Les commandes utiles

Ces commandes sont très utiles lorsque vous souhaitez **tester rapidement des modifications sans redéployer** continuellement.
Elles sont une alternative aux fichiers yaml propres, lisibles et versionnés.

Vous comprendrez vite que ce n'est pas une *bonne* idée d'utiliser ces commandes pour gérer votre cluster.

#### kubectl create

`kubectl create` vous permet de **déployer des objets en ligne de commande.** 

Par exemple:  

```bash
# Créer un namespace
$ kubectl create namespace test
# Créer un deployment d'une image nginx
$ kubectl create deployment --namespace test --image nginx nginx-test
```

#### kubectl expose

`kubectl expose` permet de **créer un *Service* pointant vers des *Pods***

Par exemple, faire pointer un *Service* de type `ClusterIP` vers le port 80 des pods déployé par le *Deployment* appelé `nginx-test`.

```bash
$ kubectl -n test expose deployment nginx-test --port=80 --type=ClusterIP
```

#### kubectl edit 

`kubectl edit` permet d'**éditer les objets présents dans le cluster**. 

Vous souhaitez éditer votre *Deployment* `nginx-test` dans le *Namespace* `test` ?

```bash
$ kubectl -n test edit deployment nginx-test
```

## La pratique !

### Installation du Nginx Ingress Controller 

Avec Helm, tout est *"simple"*. Installons le [nginx-ingress Controller](https://github.com/helm/charts/tree/master/stable/nginx-ingress) dans le namespace kube-system.

```bash
$ helm install --namespace kube-system --name nginx-ingress stable/nginx-ingress --set rbac.create=true
```

C'est fait, attendez maintenant que le *Service* de type *LoadBalancer* soit créé.

```
$ kubectl --namespace kube-system get service nginx-ingress-controller -owide  -w
NAME                       TYPE           CLUSTER-IP     EXTERNAL-IP                        PORT(S)                      AGE   SELECTOR
nginx-ingress-controller   LoadBalancer   10.3.115.207   6lhjdov8.lb.c4.gra.k8s.ovh.net   80:31281/TCP,443:31623/TCP   10d   app=nginx-ingress,component=controller,release=nginx-ingress
```

L'`EXTERNAL-IP` de votre *Service* est le DNS du LoadBalancer créé chez OVH.

### Déclaration du CNAME

A vous de jouer, **ajoutez une entrée DNS de type CNAME pour faire pointer vers votre *Service***. 
Si vous ne savez pas faire, référez vous à la documentation de l'entité qui gère vos domaines.

[Voici celle d'OVH, par exemple](https://docs.ovh.com/fr/domains/editer-ma-zone-dns/).

Dans mon cas, j'ajoute 2 CNAME
 * `kubernetes.obtao.com` qui pointe vers `6lhjdov8.lb.c4.gra.k8s.ovh.net.`
 * `*.kubernetes.obtao.com` qui pointe vers `kubernetes.obtao.com.`

Ca y est, votre **Nginx Ingress Controller recevra TOUT le traffic http et https** sur votre nom de domaine et ses sous-domaines.
Une fois les entrées DNS propagées, vous devriez voir apparaitre la page par défaut : `default backend - 404`

![Default backend](./img/default-backend.png)

Vous l'aurez compris, nous allons utiliser les Ingress pour créer virtuellement des sous-domaines. 


### Test de l'ingress controller

Pour apprendre, nous allons utiliser la commande `kubectl create`. 
Elle permet de créer rapidement des objets, sans avoir à écrire le *yaml*

#### Création d'un deployment nginx 

Créons un nginx le plus simple possible : un *Namespace*, un *Deployment* qui déploiera 1 *Pod* `nginx`.  

```bash
$ kubectl create namespace test
$ kubectl create deployment --namespace test --image nginx nginx-test
```

#### Affichage du contenu en port-forward

Affichons le contenu de ce nginx via ce bon vieux `port-forward` pour le faire pointer vers [http://localhost:8000/](http://localhost:8000/)

```bash
$ export POD_NAME=$(kubectl -n test get pods -o jsonpath="{.items[0].metadata.name}") && kubectl -n test port-forward $POD_NAME 8000:80
```

Vous devriez voir apparaitre la page d'accueil nginx.

#### Exposition d'un service via Ingress

Utilisons maintenant un *Service* et un *Ingress* pour exposer ce contenu.

```
$ kubectl -n test expose deployment nginx-test --port=80 --type=ClusterIP
$ echo "
 apiVersion: extensions/v1beta1
 kind: Ingress
 metadata:
   annotations:
     kubernetes.io/ingress.class: nginx
   name: nginx-test
 spec:
   rules:
     - host: nginx-test.kubernetes.obtao.com
       http:
         paths:
           - backend:
               serviceName: nginx-test
               servicePort: 80
             path: /
 " | kubectl -n test create -f -
```

Modifiez l'*Ingress* pour utiliser le sous-domaine que vous souhaitez, et sauvegardez. 

Si vous avez bien renseigné le nom de domaine, vous pouvez accéder à votre nginx via votre sous-domaine.

En cas de problème : 

```bash
$ kubectl -n test get ingress -owide
$ kubectl -n test edit ingress nginx-test
```


### Protection par mot de passe

Parfois, il est pratique de **protéger les ingress par des mots de passe**. 
Grâce à notre nginx-ingress Controller, c'est possible. 

#### Création d'un secret contenant des basic auth

Créez en local un fichier `auth` contenant un user `admin`. 
(Attention, le nom du fichier a son importance)

Puis envoyons directement ce fichier dans le cluster.

```bash
$ htpasswd -c auth admin
$ kubectl -n test create secret generic auth-admin --from-file=auth -o yaml
```

#### Utilisation dans l'ingress

Editez l'*Ingress* 

```bash
$ kubectl -n test edit ingress nginx-test
```

Pour y ajouter 3 annotations :

```yaml
metadata:
    annotations:
    	#Utilisation d'une authentification de type Basic
        nginx.ingress.kubernetes.io/auth-type: basic
        #Utilisation du fichier de secret nouvellement créé
        nginx.ingress.kubernetes.io/auth-secret: auth-admin
        #Message à afficher
        nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
```

Retournez sur votre nginx, un user/mdp vous est demandé.
La protection n'est certes pas parfaite, **mais elle est simple à mettre en place**.

Ne supprimez pas tout de suite ce nginx, nous allons l'utiliser dans la prochaine partie. 

[Passez maintenant vos Ingress en https](./6_cert_manager.md).
