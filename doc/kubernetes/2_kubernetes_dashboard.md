# Visualiser l'état du cluster

L'utilité du dashboard est simple : **visualiser l'état du cluster**. 
Si vous n'aimez pas la ligne de commande, il sera votre meilleur ami. 

## Théorie

### Objets

Vous vous souvenez ? Tout est *Objet*. 
Nous allons commencer par apprendre de **nouveaux concepts d'objets** et **leur utilité**.

#### ServiceAccount

Le but de ces objets est de **fournir des moyens d'authentifier** des pods/utilisateurs et de leur associer des rôles (et des droits sur les différentes API du cluster).

Pour commencer, nous allons donc créer un objet de type *ServiceAccount* qui servira à la **connection au dashboard**.
Pour être tranquilles, nous donnerons [les droits d'admin](../../kubernetes/dashboard/serviceaccount-dashboard.yaml).


#### Le pod / Le service

Ensuite, nous allons déployer le "serveur". 
Si vous avez bien suivi la théorie sur Kubernetes, vous le savez, nous allons déployer un *Pod* `dashboard`. Son utilité est de **répondre à toutes les requêtes de l'application**.
Enfin, nous allons déployer un *Service* pour **router le trafic vers le *Pod***


### Les commandes utiles

Pour le moment, nous n'abordons pas le routage du trafic depuis internet. (Les *Ingress*)
Nous allons donc utiliser l'une des 2 commandes fournies par Kubernetes pour atteindre un container : `kubectl proxy` et `kubectl port-forward`.

Et nous aurons besoin de quelques commandes pour vérifier que tout ce que nous déployons tourne. 

#### Kubectl apply

Cette commande sert à **déployer des objets dans le cluster**. (que ce soit un **fichier local**, un **dossier**, ou un **fichier distant**)

```bash
$ kubectl apply -f kubernetes/dashboard/serviceaccount-dashboard.yaml
```

#### Kubectl get 

Votre nouvelle meilleure amie : cette commande permet de récupérer les listes d'objets déployés dans le cluster. 
Et rappelez-vous : **tout est objet dans Kubernetes**.

Vous pouvez donc littéralement **TOUT lister**. 

Voici quelques exemples bien utiles (que vous pouvez essayer, si votre cluster est opérationnel) : 

```bash
$ kubectl --namespace=kube-system get pods        # Liste tous les pods d'un namespace
$ kubectl --namespace=kube-system get services    # Liste tous les services d'un namespace
$ kubectl --all-namespaces get services    		# Liste tous les services, de tous les namespaces
$ kubectl --all-namespaces get all				# Liste tous les services/deployments/replicasets/pods de tous les namespaces
```

Pour plus de détails, consultez [la doc kubernetes](https://kubernetes.io/fr/docs/reference/kubectl/cheatsheet/#visualisation-et-recherche-de-ressources) (cette partie est en français !) 

#### Kubectl describe

Voir la liste des objets, c'est pratique ! 
Mais savoir comment ils sont configurés est aussi très utile pour débugguer.
 
La commande `describe` est justement là pour cela : c'est la version (très) verbeuse du `kubectl get`. 

Vous y trouverez l'objet tel qu'il est inscrit dans la base du cluster, avec tout son paramétrage et ses données.

```bash
#Ces deux commandes affichent la description de l'object service kube-dns du namespace kube-system
$ kubectl --namespace=kube-system describe svc/kube-dns
$ kubectl --namespace=kube-system describe service kube-dns
```

Vous le voyez, les objets ont aussi leurs petits alias qui peuvent être très utiles. (`pvc` est tout de même bien plus facile à écrire que `persistentvolumeclaim`)

```bash
pod=po
service=svc
deployment=deploy
replicaset=rs
```

#### Kubectl delete

Vous l'aurez deviné, cette commande sert à supprimer des objets. 
Pas besoin de plus de détail.

```bash
kubectl --kube-system delete pods kubernetes-dashboard-65c76f6c97-dcv6d 
```


#### Kubectl proxy 

C'est la commande la plus simple. Comme son nom l'indique, elle créée un proxy entre votre local et l'intérieur du cluster. 
Une fois la commande `kubectl proxy` lancée dans un terminal vous accèderez aux *Services* de votre application via une URL. 

Par exemple pour le Dashboard: 

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy)


#### Kubectl port-forward

Le principe est le même, mais ici basé entre un port du local et un port d'un objet dans le cluster
Nous l'utiliserons principalement pour mapper des ports sur des pods. 

```bash
kubectl --namespace kube-system port-forward $POD_NAME 8443
```


## La pratique !

Allons-y pour le dashboard. 

### ServiceAccount 

Tout d'abord, déployez un *Serviceaccount* qui nous servira pour nous connecter  : 

```bash
$ cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin
  namespace: kube-system
EOF
```

Et récupérez le *Token* généré, il nous servira plus tard. 


```bash
# Trouvez le nom du secret créé par votre serviceaccount (il commence par admin-token-)
$ kubectl --namespace=kube-system get secret
# Visualisez son contenu
$ kubectl --namespace=kube-system describe secret admin-token-9qkbg
# Copiez la valeur du champ "token"
```

Si vous aimez les commandes compliquées mais sur une ligne :

```
$ kubectl -n kube-system describe $(kubectl -n kube-system get secret -o=name | grep ^secret/admin-token) | grep token: | sed 's/token: *//g'
```


### Dashboard 


Puis déployez le *Pod* du Dashboard. (Allez voir ce fichier, il contient plusieurs descriptions d'objets : *Service*, *Deployment*, ...)

```bash
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```

A partir de maintenant, votre pod est entrain de se déployer. (Et le temps de lire cette phrase, il est même probablement déployé.)

```bash
# Cette commande devrait vous afficher un pod kubernetes-dashboard en running depuis quelques secondes.
$ kubectl --namespace=kube-system get pods 
```


Voila. Votre dashboard est installé, mais... inaccessible. Vous n'avez aucun point d'entrée depuis l'extérieur du cluster pour visualiser son contenu.


### Kubectl proxy

Pour visualiser le dashboard, lancez cette commande qui va lancer un proxy entre votre local et le cluster :

``` bash
$ kubectl proxy 
Starting to serve on 127.0.0.1:8001
```

C'est tout, vous pouvez désormais accéder à l'interface du dashboard en utilisant [cette URL](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy).

Vous vous souvenez le token créé/récupéré à la première étape ? Utilisez le, et profitez. 
Tout le dashboard est basé sur les API que vous pouvez utiliser via `kubectl`. Essayez de vous y retrouver :-)

![Dashboard Kubernetes Login](./img/Kubernetes-dashboard-login.png)
 
### Kubectl port-forward

Personnellement je préfère *port-forward*, pour sa simplicité d'utilisation dans le navigateur. 

Tout d'abord, récupérez le nom du pod courant qui contient le dashboard. 
Vous pourriez aussi utiliser un `kubectl get` puis un copier/coller, mais souvenez vous : le *Pod* est mortel, il changera de nom si il est détruit...

```bash
$ export POD_NAME=$(kubectl get pods --namespace kube-system -l "k8s-app=kubernetes-dashboard" -o jsonpath="{.items[0].metadata.name}")
$ kubectl --namespace kube-system port-forward $POD_NAME 8443
```

[Utilisez cette URL](https://localhost:8443/)

Tant que le proxy/port-forward est ouvert, votre dashboard est accessible. 

![Dashboard Kubernetes Home](./img/kubernetes-dashboard-home.png)


### Kubectl delete

Allez, votre côté destructeur vous pousse à le faire depuis tout à l'heure. 
Vous pouvez y aller et supprimer tout ce qui a été créé par la commande `kubectl apply`.

```bash
$ kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```


**BRAVO !**

Vous pouvez maintenant [installer une première application.](./3_premiere_app.md)
