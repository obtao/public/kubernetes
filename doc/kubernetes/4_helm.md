# Simplifier l'installation

## Théorie

### Problème

Nous l'avons vu, déployer dans Kubernetes est assez simple. 
Décrire l'infrastructure dans des fichiers `yaml`, les déployer avec un `kubectl apply -f `, et c'est réglé.

Dans la pratique, **une application se retrouve vite à déployer plus d'une dizaine d'objets**.
Par exemple, un serveur de fichier statique déploiera : 

* *Deployment* / *Replicaset* / *Pod* pour assurer la fonction de serveur
* Un *Service* pour accéder aux *Pods*
* Un *Ingress* pour accéder au *Service* depuis l'extérieur
* Un *ServiceAccount* pour authentifier les *Pods* (Qui ne servirait probablement à rien, mais c'est une bonne pratique à prendre) 

Ajoutez à cela un serveur *php-fpm* (*Pod*/*Service*/*ConfigMap*), et nous sommes déjà à 7 objets pour une application Web Symfony "basique".

Je pourrais vous dire que c'est impossible. Mais c'est faisable, et largement gérable. 

Par contre, si on ajoute la possibilité de déployer votre application dans différents environnements (preprod/prod) :
* Sur **différents DNS**
* Avec différentes **ressources** (+/- de RAM ou de CPU)
* Avec différents **paramétrages applicatifs**
* Avec un nombre de **replica** différent.
* Utilisant des services comme **Elasticsearch** ou **Redis**

Là, vous devriez suer des gouttes. 
Vous imaginez certainement un **système de fichiers `yaml` templatisés** pour ce qui est variable ? 
Et pour les services complexes, un petit copier/coller de l'Internet, et le tour est joué ? 
Bravo.


### Solution 

Le **templating** est justement ce qui fait la force de Helm. 
Ca et les **dizaines de *charts* disponibles** (*Elasticsearch*, *Redis*, *Mongodb*, *PostgreSQL*, ...)

Helm se veut le **"package manager"** de Kubernetes. 
En clair, il permet la gestion des applications dans un cluster :
- **Configuration**
- **Installation**
- **Mise à jour**
- **Rollback**

Helm est composé de 2 parties : 
* Un **client**, installé sur **votre machine**
* Un **serveur**, ou *Tiller* installé **dans votre cluster**. 

Vous verrez dans la partie *Symfony* de cette série d'articles comment écrire votre propres *charts*.
Nous allons pour l'instant utiliser Helm comme installateur de charts externes. 

### Commandes utiles

#### Helm init

Cette commande permet au client d'**installer la partie serveur (Tiller)**. 

```bash
$ helm init
```

#### Helm install

Comme son nom l'indique permet d'**installer un *chart* dans votre cluster**. 
La commande est utilisée pour déployer un chart local, ou un chart externe.

Vous pouvez passer un fichier de values, qui n'est ni plus ni moins que l'ensemble des valeurs passées aux templates.

```
$ helm install --name ma-release --namespace mon-namespace mon-dossier-local/
$ helm install --namespace=monitoring --name prometheus-adapter stable/prometheus-adapter -f kubernetes/prometheus-adapter/values.yaml
```

#### Helm ls

`helm ls` vous permet de **lister les releases installées** dans votre cluster. 

```bash
# Afficher les release en status DEPLOYED
$ helm ls
# Afficher toutes les releases, peu importe leur status
$ helm ls --all
```

#### Helm upgrade

Une fois votre application installée, vous allez probablement vouloir **la mettre à jour**. 
Le plus simple est d'utiliser la commande `helm upgrade`. 

Derrière, c'est surtout `kubernetes` et son rolling-update qui travaille. 
Mais Helm vous permettra de revenir *très* simplement en arrière en cas de problème.

```bash
$ helm upgrade --install --namespace mon-namespace ma-release ./mon-dossier-local 
```

Notez l'utilisation de `--install`, permettant d'installer automatiquement l'application si elle n'est pas encore présente dans le cluster. 

#### Helm rollback

Votre déploiement s'est mal passé et termine en *FAILED* ? Vous vous rendez compte que votre application ne fonctionne plus ?
Helm rollback **permet de revenir à un release précédente** (disons la 12). 

```bash
$ helm rollback ma-release 12
```

Attention tout de même, les fichiers ajoutés par des releases qui terminent en *FAILED* ne sont pas supprimés.
Et ils pourront vous causer quelques soucis au prochain déploiement. 

#### Helm delete

Vous souhaitez **désinstaller totalement une application/un service** ? 

```bash
# Pour marquer la release comme DELETED
$ helm delete mon-application
# Pour tout recommencer à zéro
$ helm delete --purge mon-application
```


## La pratique !

Pour l'instant, nous n'allons installer que le *Client*, le *Tiller* et un nginx de test.


### Installer le client Helm

Rien de tel que la [documentation officielle](https://helm.sh/docs/using_helm/#installing-helm).

Mais : 
* Sous Mac OS X : `brew install kubernetes-helm`
* Sous Linux : `curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash`


### Installer le tiller Helm

Attention, tout va aller très vite. 

Créons tout d'abord un compte de service Kubernetes pour donner les droits à Helm.

```bash
$ echo "
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
" | kubectl create -f -
```

Installons le serveur Helm dans le cluster Kubernetes.

```bash
$ helm init --history-max 200 --service-account=tiller
```

Helm aura, entre autres, installé un *Pod* tiller dans votre cluster. 
Vous pouvez le voir fonctionner.

```bash
$ kubectl -n kube-system get pods | grep tiller
tiller-deploy-7b65c7bff9-chb6j                   1/1     Running   0          4d12h
```

### Installer un wordpress

Si vous avez déjà installé un wordpress, vous savez de quoi on parle. 
Rien de compliqué, mais rien de très intéressant non plus :
1. Installer **un serveur**
2. Installer **une bdd**
3. Installer **php/nginx**
4. Installer **wordpress**
5. Prier pour qu'il ne plante jamais

Avec Helm et Kubernetes, la philosophie est différente. Pour que votre blog fonctionne, vous n'aurez pas grand chose à faire.
Soyons honnêtes, à la fin de cette installation vous ne comprendrez pas tout... Et pourtant il tournera ! 
Et sera peut être **mieux paramétré qu'un wordpress installé à la main**. (Désolé pour votre égo)


#### Installer wordpress

Pour installer Wordpress, il exite un Chart Helm. 
Il vous suffit donc de demander l'installation dans votre cluster. 

```bash
helm install --namespace wordpress-test --name wordpress-test stable/wordpress --set mariadb.master.persistence.enabled=false,persistence.enabled=false,service.type=ClusterIP
```

Vous remarquerez l'utilisation de `--set mariadb.master.persistence.enabled=false,persistence.enabled=false`. 
Par défaut, ce chart installe des **volumes persistents** pour ne pas perdre les données. Le déploiement de ces volumes créent automatiquement des **volumes OVH**. (Eh oui !)

Le problème est qu'**ils coûtent quelques centimes par Gio par mois**. Et qu'il n'est pas vraiment utile de les instancier pour un test.

Attendez maintenant que les *Pods* Wordpress tournent (Un serveur wordpress, et un serveur mariadb).

```bash
$ kubectl -n wordpress-test get pods -w
```

#### Visualiser votre Wordpress

Lancez le port forward pour admirer [votre travail](http://localhost:8000) !

```bash
$ export POD_NAME=$(kubectl -n wordpress-test get pods -l "app=wordpress-test-wordpress" -o jsonpath="{.items[0].metadata.name}") && kubectl -n wordpress-test port-forward $POD_NAME 8000:80
```

![Wordpress install](./img/wordpress.png)


Bravo. Vous en êtes à un cluster Kubernetes d'installé, et un Wordpress déployé via Helm.


[Et si on arrêtait les port-forward ?](./5_nginx_ingress_controller.md)


