# Installation du cluster Kubernetes (OVH)

Le choix se porte pour l'instant sur **OVH**. 
Mais vous pouvez sans problème créer votre cluster chez *Google Cloud Platform*, *AWS* (EKS) ou *Azure* (AKS).


## Kubernetes

Vous trouverez des dizaines d'articles sur Internet à propos de Kubernetes (*k8s* pour les intimes).
En résumé, Kubernetes est un orchestrateur de containers Docker propulsé par Google.

Comme tout ce qui sort de chez Google, la techno est **impressionnante**, **bourrée de possibilités**, mais **complexe et verbeuse**.

La meilleure documentation reste cependant probablement la [documentation officielle](https://kubernetes.io/). 
Il va falloir, malheureusement, commencer par un peu de théorie.


### Master/Node

Kubernetes est établi sur un principe simple : des serveurs *master* hébergent la configuration du cluster et le gèrent. 
Des serveurs *Nodes* viennent héberger tous les "objets" déployée dans le cluster (Vos services/applications)

Pour l'instant, vous n'êtes pas concernés car **OVH gère les master pour vous**. 
Mais sachez tout de même que si vous perdez vos masters, vous perdez définitivement la possibilité de toucher à votre cluster. (D'où l'intérêt d'une solution managée...)

Toutes les opérations sur le cluster utilisent des API, et vous allez intéragir avec les master via la commande `kubectl`

### Les objets

Les objets représentent **votre infrastructure logicielle dans un cluster Kubernetes**. 
Ces objets sont, la plupart du temps, représentés installés par des fichiers `yaml`. 

Dans Kubernetes, **TOUT est objet** (Même les *Nodes* sont des objets !). 
C'est peut être un détail pour l'instant, mais pour moi ca veut dire beaucoup.

Voici donc les *objets* les plus simples à comprendre et les plus utilisés.

#### Pod

C'est le plus petit objet Kubernetes. 
Un pod est un **ensemble de containers déployés dans une bulle commune** (le routage de trafic entre 2 containers d'un même pod communiquent par `localhost`). 
Il est **réplicable** selon les envies/besoins.

Vous pouvez comparer un pod à un serveur dans une architecture classique.
Vous pourriez décider par exemple de créer un *Pod* `nginx/php-fpm`.

Comprenez bien que ces *Pods* sont **mortels**, d'après la documentation. 
Un fichier, un log, ou tout autre contenu déposé sur un container dans un *Pod* est éphémère.
Si ce concept n'est pas différent de docker, il est encore plus poussé dans Kubernetes. **Les pods, tout comme les nodes, sont *mortels*, ne vous y attachez pas. **

#### Service

Grossièrement, les services Kubernetes servent à représenter les *Pods*.
La plupart du temps, vous placerez un *Service* devant un type de *Pod*. 

Si l'on reprend l'exemple des *Pods* `nginx/php-fpm`, vous placerez un *Service* `nginx/php-fpm` permettant de contacter à coup sur l'un des *Pods* en cours d'exécution.

On peut comparer un *Service* à un load balancer, qui est très utile puisque les pods placés derrières peuvent être supprimés à tout instant.

#### Ingress

C'est un point d'entrée depuis l'extérieur. Vous y définissez des règles qui permettent à l'*Ingress controller* de gérer le traffic http/https vers des *services* Kubernetes.

![Ingress, Service et Pods](./img/ingress_service_pod.png)
(Diagramme par Ahmet Alp Balkan)


#### Namespace

C'est un **cloisonnement virtuel**. Ni plus, ni moins. Vous déploierez toujours vos objets dans des namespaces. 
Il existe 2 *Namespaces* installés par défaut :
 * `default` : Si vous ne précisez rien, c'est ici que vos applications seront installées 
 * `kube-system` : Vous y trouverez les *Pods*, *Service* et objets Kubernetes.
 
 A vous de choisir, mais nous vous conseillons d'utiliser un namespace par projet.

#### Les autres

Il existe des dizaines d'autres objets Kubernetes (vous pouvez même en charger de nouveaux)
Leur utilité varie : 

* Déclarer un traitement déclenché manuellement : *Job*
* Déclarer un traitement récurrent : *Cronjob* 
* Assurer la disponibilité des pods dans une version donnée : *Replicaset*
* Assurer le déploiement et la mise à jour des versions de Replicaset : *Deployment*
* Déclarer un volume de stockage : *PersistentVolume*
* ...

Vous apprendrez petit à petit à appréhender les subtilités des objets, et toutes leurs possibilités. 

Tout sera bientôt clair sur le fait qu'un *Deployment* réalise la transition d'un *Replicaset* à un autre, que ceux-ci déploient des *Pods*, accessibles via un *Services* à l'intérieur du cluster et un *Ingress* depuis l'extérieur.


## La pratique

### Le cluster 

Pour créer votre cluster, rendez-vous dans [votre compte OVH](https://www.ovh.com/fr/order/express/#/express/review?products=~(~(productId~'cloud~planCode~'project.2018~configuration~(~(label~'description~values~(~'Kubernetes*20project))))~(productId~'kubernetes~planCode~'kubernetes-managed-cluster~(~(label~'name~values~(~'Kubernetes*20cluster))))))

Je vous invite au passage à utiliser les codes promos (*vouchers*) `CLOUD`, `OPENSTACK-PASSPORT` et `K8S`, qui vous permettront d'économiser entre 30€ et 50€ sur vos factures. (Et donc de mener à bien vos tests **Gratuitement**)

Pour le moment OVH vous offre les masters, vous ne paierez donc que les noeuds. 
Vous pouvez en créer deux, pour cet exemple. (**2 instances B2-7 :  0,062 € HT / heure chacune**)

![OVH création des nodes](./img/cluster-ovh-nodes.png)

**Attention** : Vos actions peuvent générer des coûts supplémentaires. 
L'installation d'un *PersistentVolume* par exemple, créera des Volumes **OVH Cloud** : **0,08 € HT / Gio / Mois**

Petit conseil au passage, si votre cluster est la uniquement pour des tests, n'hésitez pas à **supprimer les nodes quand vous ne vous en servez pas** !
La configuration étant présente sur les master, à l'ajout d'un ou plusieurs noeuds, tout va se réinstaller automatiquement (eh oui, c'est ça aussi la résilience).

### Kubectl 

![OVH création du cluster](./img/cluster-ovh-vue.png)

* Téléchargez le fichier `kubeconfig` depuis l'interface OVH Cloud Kubernetes et placez le ici : `~/.kube/config`. Il sera utilisé par défaut pour toutes les commandes lancées par la suite.

* [Installez le client Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Exécutez cette commande : `kubectl get nodes`

```bash
$ kubectl get nodes
NAME      STATUS   ROLES    AGE   VERSION
node-01   Ready    <none>   9d    v1.12.7
node-02   Ready    <none>   9d    v1.12.7
```

**Votre cluster est opérationnel.**  
Vous pouvez maintenant [installer le dashboard](./2_kubernetes_dashboard.md)
