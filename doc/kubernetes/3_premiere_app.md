# Installation de votre première application

Vous avez un cluster, vous avez un dashboard. (Si vous ne l'avez pas supprimé) 
Apprenons maintenant à déployer une application `php/nginx`.

Pour l'instant nous utiliseront une **application démo de blog** ce qui permettra de faire abtraction du **développement** et de partie **construction des images**.

Vous pouvez récupérer cette application [ici](https://www.gitlab.com/obtao/blog-demo.git) si vous souhaitez en savoir plus.

Retenez simplement que cette application utilise : 

* `php-fpm`
* `nginx`
* `PostgreSQL`

Nous allons mettre en place **tous les objets Kubernetes permettant d'afficher ce blog**.

## Théorie

### Objets

#### Replicaset

Vous connaissez le principe des *Pods*. et des *Services*. 
Pour **déployer 3 serveurs** `php-fpm`, vous pourriez créer **3 fichiers yaml pour déployer 3 *Pods***. 
La maintenance, comme les déploiements, s'annoncent fastidieux. 

Le but du *Replicaset* est de faciliter la création de *Pods* identiques en utilisant un **template**.
Et c'est tellement pratique que vous n'avez aucune raison de vous en passer, et que vous ne créerez quasiment jamais de *Pod* à la main. 

Mais vous ne créerez probablement jamais de *ReplicaSet* à la main. 

#### Deployment

Si les *ReplicaSet* sont utiles pour déployer des *Pods* identiques, **ils ne savent pas gérer leur mise à jour**.

Pour mettre à jour les *Pods* d'un *ReplicaSet*, vous devrez **déployer un second *ReplicaSet***, et **supprimer le premier**.

C'est ce problème que solutionne les *Deployment*. Ils viennent **gérer la mise à jour progressive de votre application**. 

**Prenons un exemple concrêt :**
 
Vous avez installé un *Deployment* qui a déployé un *Replicaset* (V1) de 10 *Pods* de votre application en version `1.0`.

Via le yaml de ce *Deployment* vous allez pouvoir paramétrer le déploiement progressif/automatique de la mise à jour en version `2.0`.


1. Déployer un *ReplicaSet* V2 avec 2 *Pods* (**V1** : 10 *Pods* ready, **V2** : 2 *Pods* en cours de création)
2. Eteindre 2 *Pods* de la V1 lorsque les 2 premiers *Pods* de la V2 seront prêts et en créer 2 nouveaux (**V1** : 8 *Pods*, **V2** : 2 *Pods* ready + 2 *Pods* en cours)
3. Ainsi de suite jusqu'à obtenir un déploiement à 100% en **V2**
4. Détruire le *ReplicaSet* **V1**

Il saura également stopper le déploiement si la mise à jour en **V2** se passe mal. 
(Et grâce au logs, vous devriez savoir pourquoi).

### Commandes

#### Kubecetl logs

Comme pour docker, il est possible d'afficher les logs d'un container. 

La seule spécificité ajoutée par Kubernetes, est que l'on affiche les logs de *Pods*, mais qu'il est parfois nécessaire de préciser le container ciblé.

```bash
$ kubectl logs [nom-dun-pod]
$ kubectl logs -f [nom-dun-pod]
$ kubectl logs --since="1m" -f [nom-dun-pod]
$ kubectl logs -f deployment/nginx
$ kubectl logs -f [nom-dun-pod] -c [nom-dun-container]
```

#### Kubectl exec

Comme pour docker, il est possible d'entrer dans un container ou d'executer des commandes. 

```bash
$ kubectl -n monitoring exec -ti [nom-dun-pod] sh
$ kubectl -n monitoring exec -ti [non-dun-pod] -c [nom-du-container] sh
``` 


## La pratique

Plutôt que vous décrire le contenu des fichiers, je vous invite à aller le voir directement. ([ici](../kubernetes/app))

```bash
# Créez le namespace
$ kubectl create namespace blog-demo
# Installer l'application
$ kubectl -n blog-demo apply -f kubernetes/app
# Jouer la création de schema via Symfony une fois tous les pods running
$ export POD_NAME=$(kubectl get pods --namespace blog-demo -l "app=blog-demo,serviceType=php" -o jsonpath="{.items[0].metadata.name}") && kubectl -n blog-demo exec $POD_NAME -- bin/console d:s:u --force
# Lancer port-forward
$ export POD_NAME=$(kubectl get pods --namespace blog-demo -l "app=blog-demo,serviceType=nginx" -o jsonpath="{.items[0].metadata.name}") && kubectl -n blog-demo port-forward $POD_NAME 8000:8000
```

**Allez-y, visitez [localhost:8000](http://localhost:8000)**

![Dashboard blog](./img/blog-home.png)

Maitenant que votre blog tourne, installé par des fichiers `yaml`, vous pouvez [passer à l'installation de Helm](./4_helm.md)
