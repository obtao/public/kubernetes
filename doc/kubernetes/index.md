# Deploiement d'une application Symfony dans un cluster Kubernetes via Gitlab-ci

## Contexte

Déployer sous docker sur un VPS pour faire quelques tests, c'est bien. 
Mais nous sommes loin de la haute disponibilité requise pour des applications destinées à la production. 

Pour faire les choses correctement, il vous "suffit" de : 
* commander un **pool de serveur**
* les **configurer** (nginx/apache, php, logrotate, avec Ansible pour pouvoir reproduire les installations)
* gérer la **remontée des logs** (Kibana, Logstash, Elasticsearch)
* Configurer des **alertes** (Disk full, Server down, ...)
* Configurer un **déploiement continu** (Capistrano ?)
* Prévoir la **maintenance** et la **mise à jour** des services. Ne nous leurrons pas, l'installation d'une 2ème application sur vos serveurs sera tout aussi complexe.

En résumé : quelques litres de café, 3 semaines de travail et le tour est joué. En fait, adminsys, c'est un métier ! 
(Et je vous ne souhaite pas de réussir, la scalabilité s'annonce laborieuse)


## Une seule solution 

Le but de cet article est de profiter de votre nouvelle maitrise en terme de génération de containers docker. 
Mais surtout, de profiter de **Kubernetes** pour produire en quelques heures un **cluster totalement configuré** avec **votre application déployée**.

Dans le détail, voici les technos qui vont entrer en jeu : 

* [Le Cloud Public OVH](./1_cluster_kubernetes_ovh.md)
* [Kubernetes : l'orchestrateur de containers](./1_cluster_kubernetes_ovh.md)
* [Helm : Le package manager pour Kubernetes](./4_helm.md)
* [Fluentd : Récupérateur de logs](./7_logs.md)
* [Elasticsearch : Aggrégateur de logs](./7_logs.md)
* [Kibana : Lecture des logs / Dashboards](./7_logs.md)
* [Prometheus : Aggrégateur de métriques](./8_prometheus.md)
* [Grafana : Affichage des métriques / Dashboards](./8_prometheus.md)
* [Nginx Ingress Controller : Routeur de trafic dans le cluster](./5_nginx_ingress_controller.md)
* [Cert Manager : Gestion des certificats pour le https](./6_cert_manager.md)
* [Nginx / PHP-fpm 7.3 : Serveurs web/php](./3_premiere_app.md)
* [Gitlab : Repository git, accompagné de gitlab-ci/cd pour le déploiement/intégration continue](./9_gitlab.md)

A la fin de cette série de tutoriels, vous aurez une application Symfony déployée en haute dispo dans un cluster Kubernetes. 
Les logs seront visualisables dans Kibana, et les métriques dans Grafana.


### Prérequis

* Un **compte OVH** (Vous bénéficierez de 60 € d'essai, largement de quoi travailler)
* Une **application en Symfony 4** (qui tournera en PHP 7.3)
* Des **connaissances de base en docker**

Et c'est tout. 

Dans l'absolu, vous pourriez travailler en local et créer un cluster Kubernetes sur votre machine : le tutoriel ne changerait pas.
Mais avouez que **déployer dans le cloud, c'est la classe**.

Si vous souhaitez quand même installer votre cluster en local : 
* Sous Mac OS X, utilisez le serveur Docker pour activer Kubernetes (`Enable local cluster` dans le menu `kubernetes`)
* Sous Linux, [installez Minikube](https://kubernetes.io/docs/setup/minikube/).

Il existe des dizaines d'autres manières de créer un cluster Kubernetes, je vous laisse les découvrir.

### Le fonctionnement 

Vous allez installer pas à pas votre cluster, gérer le déploiement et l'intégration continue, installer votre application et paramétrer son infrastructure.
A chaque étape, nous verrons l'utilité de chaque service utilisé, et comment le paramétrer le plus simplement pour qu'il assure la fonction choisie.

### Disclaimer

Ce tutoriel ne fera pas de vous un expert Kubernetes, ce n'est pas non plus une documentation exhaustive.
Il fait même probablement l'impasse sur quelques points de sécurité.

Et au passage, ce tutoriel est bourré de choix personnels (technos et organisation de dépôts notamment). 

## Sommaire

1. [Comprendre Kubernetes et installer un cluster OVH](1_cluster_kubernetes_ovh.md)
2. [Visualiser l'état de votre cluster avec le Dashboard](2_kubernetes_dashboard.md)
3. [Installer votre première application](3_premiere_app.md)
4. [Installer Helm](4_helm.md)
5. [Installer Nginx Ingress Controller](5_nginx_ingress_controller.md)
6. [Installer Cert Manager](6_cert_manager.md)
7. [Installer la stack EFK](7_logs.md)
8. [Installer Prometheus](8_prometheus.md)
9. [Installer Gitlab](9_gitlab.md)
9. Créer des alertes avec AlertManager ?
10. Traefik ?




