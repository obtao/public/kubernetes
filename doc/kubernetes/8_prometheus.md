# Mesurer les métriques

## Le problème à résoudre

Vos applications tournent et **vous récupérez les logs de vos applications**.
Cela vous permettra surtout de lire et d'analyser les comportements de vos containers.

Même si Kibana peut créer des dashboards basés sur le temps, Elasticsearch n'est pas adapté pour la gestion de métriques (mais pour le stockage et la visualisation des documents et donc des logs).

Mais comme pour une infra de serveurs "classique", le cluster Kubernetes peut (et doit) être monitoré : 
* Usage des **CPUs** / **RAM** par node
* Utilisation des **disques**
* **Nombre de noeuds** disponibles
* **Consommation des *Pods***

Vous découvrirez que les applications que vous déployez sont également capables de produire des métriques :

* **PHP-FPM** : Nombre de process, file d'attente, consommation CPU, ...
* **Elasticsearch** : Nombre de noeuds, taille des indexes, mémoire, ...
* **PostgreSQL**
* **Kafka** / **RabbitMQ**
* ... 

Et il peut être utile de les suivre !

Nous allons donc installer une suite d'outils pour automatiser , récolter, analyser et afficher ces métriques : **Prometheus** (récolte/traitement) et **Grafana** (affichage). 
Au passage, nous allons également installer **AlertManager** ce qui vous permettra de... recevoir des alertes selon vos configurations. 

Vous le verrez, Prometheus *scape* les données. C'est à dire qu'il vient périodiquement appeler un exporter pour récupérer les valeurs à un instant T.

### Les Objets

Comme pour [Cert-Manager](./6_cert_manager.md) et ses *Issuer*, Prometheus embarque ses **CRD** : les *ServiceMonitor*.

Une des philosophies appréciables de Kubernetes est pour moi l'auto-discovering. 
(Que ce soit en utilisant des *CRD*, des annotations ou des labels)

**L'avantage majeur est l'automatisation et l'industrialisation.**

#### ServiceMonitor

Prometheus n'échappe pas à cette règle. 
Les objets de type *ServiceMonitor* permettent à Prometheus de savoir quels sont les *Services* Kubernetes **qui exposent un exporter**. (C'est à dire un **container qui peut lui fournir des métriques**) 
Prometheus les appelera pour récupérer ses métriques.

La configuration est assez simpliste.

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
  	#label pour automatiser la découverte par Prometheus
    release: prometheus-operator
  name: mon-app-fpm
  namespace: monitoring
spec:
  endpoints:
  #Configuration Prometheus (https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#endpoint)
  # Le port, le chemin (/metrics par défaut) et l'interval 
  - interval: 15s
    port: metrics-fpm
  jobLabel: metrics-fpm
  #Selection du service qui exporte cette données
  namespaceSelector:
    matchNames:
    - mon-app
  selector:
    matchLabels:
      serviceType: php
```

Ici, le *Service* php expose un port `metrics-fpm`. 
Ce port redirige vers un **container php-fpm-exporter** présent dans nos *Pods* php.
Prometheus collectera les métriques **toutes les 15 secondes**.

#### PersistentVolumeClaim

La notion de *PersistentVolumeClaim* a 2 sens selon la configuration du cluster.
Si **les volumes sont déclarés de manière *statiques*** par les administrateurs, alors l'objet *PersistentVolumeClaim* viendra "utiliser" un ***PersistentVolume* pour pouvoir le monter dans un *Pod***

Si les volumes sont **déclarés de manière *dynamiques*** et que notre cluster est chez OVH par exemple, alors cela devient bien plus intéressant.
La création d'un objet *PersistentVolumeClaim* vient **automatiquement** créer un *PersistentVolume* et donc un disque chez OVH.

Ces *PersistentVolumeClaim* et leurs *PersistentVolume* vont par exemple **être très utiles dans la création de base de données**.
Lors des redémarrages, crash, update vos *Pods* **re-monteront les bons volumes sans perte de données**.

#### PersistentVolume

Les *PersistentVolumes* dans Kubernetes ressemblent à des disques physiques sur une architecture *classique*.
Le but est de sauvegarder la donnée hors des *Pods* et d'éviter les pertes de données en cas de crash. **Cet objet vient ajouter une couche d'abstraction au stockage.**

Il existe de nombreux adapter pour les volumes Kubernetes, la [documentation officielle](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volumes) est bien plus détaillée

Vous l'aurez lu juste au dessus, chez OVH/GCP/AWS/Azure, **il est possible de créer automatiquement les stockages à la création d'un *PersistentVolume***.

Le cycle de vie d'un *PersistentVolume* peut varier selon la `Reclaim Policy` (Retain, Recycle ou Delete). 
Mais la plupart du temps dans nos cas, **le *PersistentVolume* sera configuré en `Deleted`.** 
C'est à dire que **le stockage OVH sera supprimé lorsque le *PersistentVolumeClaim* n'existera plus**.  

Vous devriez mieux comprendre les configurations de persistence dans certains charts Helm ([dans les logs par exemple](./7_logs.md))

### Les commandes

C'est fini ! Je n'en ai plus en stock.


## La pratique !

C'est probablement **la configuration de Helm la plus complexe que vous verrez dans cette série d'articles**. 
Mais c'est aussi la plus impressionnante en terme d'industrialisation. Voici le détail de la configuration avant de vous la faire appliquer.

Pour en savoir plus, lisez [la documentation officielle de Prometheus-Operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator)


### Prometheus

**Prometheus vient collecter la donnée et la stocker**. Les *ServiceMonitor* viendront par la suite déclarer les exporter.
Prometheus-Operator embarque par défaut un node-exporter, permettant de récupérer les métriques de vos *Nodes* Kubernetes.

La configuration du chart Helm dédié à Prometheus vient simplement créer un *Ingress* permettant d'accéder à son interface.

### AlertManager

**AlertManager est une application dédiée à l'alerting (sur base de métriques Prometheus)**.
Pour le moment rien de spécial. Comme pour Prometheus, on vient déclarer un *Ingress*.

### Grafana 

Beaucoup plus intéressante, la configuration du Grafana de Prometheus-Operator vient également :

* **Ajouter un *Ingress***
* **Activer une DataSource supplémentaire** (L'Elasticsearch de la brique dédiée au logs : [EFK](./7_logs.md)). Grafana pourra donc afficher des Dashboards sur les données de logs.
* **Ajouter 2 dashboards automatiquement** : celui de `php-fpm` et celui pour `elastic-exporter` que nous installerons plus tard sur les ES de logs.
* **Activer le sidecar** dont la mission est de **charger à la volée des Dashboards Grafana** déclarés dans les *ConfigMap* de vos application ! (Si ca ce n'est pas génial... vous êtes difficiles.)

Concrêtement, une de vos applications Symfony peut déclarer un objet *ConfigMap* contenant du JSON Grafana, et ajouter le label `graphana_dashboard: "1"`.
Les fichiers JSON décrits dans ce *ConfigMap* apparaitront directement dans le Grafana.
Cela vous permet d'industrialiser totalement la création de dashboards Grafana : **vos applications embarquent le monitoring**.

C'est pour cela que l'ES des logs est branché à Grafana : dès les premiers appels à vos applications, votre dashboard Nginx sera alimenté.


### Installation

```yaml
$ helm install --namespace=monitoring --name prometheus-operator stable/prometheus-operator -f kubernetes/prometheus/values.yaml
```


Vous pouvez également installer l'exporter pour l'Elasticsearch de la stack de logs.
Pour cela, il suffit de décommenter la partie exporter dans le fichier [values.yaml de elastic-stack](../../kubernetes/elastic-stack/values.yaml) et de mettre à jour la release.

```bash
$ helm upgrade --namespace=monitoring elastic-stack stable/elastic-stack -f kubernetes/elastic-stack/values.yaml
```

Et voila, votre prometheus est installé et couplé à AlertManager.

Grafana est lui non seulement branché **sur les métriques de Prometheus**, mais aussi branché **sur les logs de l'Elasticsearch** installé précédemment. 
(Utilisez le user / mot de passe par défaut : `admin`/`prom-operator`)

![Grafana K8S](./img/Grafana-k8s.png)

![Grafana ES](./img/grafana-es.png)

Quelques dashboards sont pré-installés : 
* **Kubernetes** (Dns, namespaces, nodes, ...)
* **Elasticsearch** : Visualisation de l'état du cluster
* **php-fpm** : dont vous aurez besoin dans la dernière partie

[Installez maintenant votre CI/CD avec Gitlab](./9_gitlab.md)
