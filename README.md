# Les containers, du local à la production

Le but de cette série d'articles et des exemples associés n'a qu'un seul but : **démocratiser l'utilisation de containers.**

Ces 5 dernières années, **l'intérêt pour Docker et ses containers n'a fait que croitre**.
Mais au départ tout le monde préconisait une **utilisation uniquement *locale* des containers**.
 
Désormais, avec les orchestrateurs, le discours a changé. Et **avec Kubernetes, c'est devenu presque conseillé**.

Si votre **équipe est de taille réduite**, que vous souhaiter **maitriser votre infrastructure**, mais que vous souhaitez vous **concentrer sur votre application**, ce dépôt est fait pour vous.

A la fin de cette série d'articles vous aurez :
* Un **environnement local** évolutif et sous docker
* Un **cluster Kubernetes** et les bases en compréhension
* Une **intégration et un déploiement continus** basés sur Gitlab
* Du **monitoring** / des **logs**
* Un **environnement en haute-disponibilité et résilient**
* Un **socle de containers** Docker (php-fpm et nginx)

Tous les **articles sont étroitement liés** et bourrés de choix assumés ! 
N'hésitez pas à **essayer**, **apprendre**, **comprendre** et nous faire vos retours. 


## Le local

Commençons par le commencement : vous souhaitez développer une **application sous Symfony**, en **local** et sous **Docker**. 

Cet article va vous permettre d'utiliser *Traefik*, et d'avancer dans la **configuration d'un environnement local**.

Vous pourrez vous inspirer de [cette application](https://gitlab.com/obtao/blog-demo) pour préparer/configurer votre application et écrire vos Dockerfile. 

Ce blog de démo est basé sur un socle [php-fpm](https://gitlab.com/obtao/docker-images/php-fpm) et [nginx](https://gitlab.com/obtao/docker-images/nginx) hébergé également dans notre Gitlab, que vous pouvez récupérer et améliorer comme vous le souhaitez. 

Allez-y ! [Configurez *Traefik* pour votre application locale](./doc/local/traefik.md).


## Les serveurs sous docker

Maintenant que vous appréciez la simplicité de configuration de *Traefik*, vous avez envie de le **déployer partout** ?

Cet article vous permettra de **déployer *Traefik* sur un serveur de démo** sous Docker, et d'y **ajouter les certificats *https***.

[Lire l'article sur les certificats https via Traefik et Let's Encrypt](./doc/test/https.md)


## Gitlab

Vous savez créer des containers, les builder/déployer sur des serveurs de démo. 
Il vous faut maintenant un endroit sûr pour **builder, tester et stocker vos containers** de manière **durable** et **automatisée**. 

Notre choix se porte sur Gitlab, **beaucoup** plus puissant en terme d'intégration continue. 
La formule de base est gratuite, et largement suffisante pour 95% des équipes. 

Nous vous expliquerons la création des socles, un *gitflow* simple et efficace, le build des containers et les tests.


[Installer/Utiliser gitlab-ci](./doc/gitlab/index.md)

## Le cluster Kubernetes

Nous y sommes! Vous savez construire vos conteneurs, mais vous voulez industrialiser vos applications ? 

Kubernetes est un orchestrateur propulsé par Google. 
Dans cette série d'articles, mélant théorie et pratique, vous apprendrez à déployer un cluster OVH tout à fait honorable et y installer :

* EFK (Elasticsarch, Fluentd, Kibana) : pour récupérer et lire les logs de tous vos containers
* Nginx Ingress Controller : Pour gérer des sous-domaines et des certificats http associés
* Prometheus/Alertmanager/Grafana : Pour gérer et lire les métriques
* Des application web sous Symfony

Il sera au final branché au Gitlab, pour terminer cette série d'article sur le déploiement de *votre* application dans *votre* cluster. 
 
[Installer mon cluster Kubernetes OVH](./doc/kubernetes/index.md)

Et si vous avez déjà un cluster Kubernetes chez GCP, AWS ou Azure, allez-y tout de même.
